from north_utils.joystick import Joystick

if __name__ == '__main__':
    joystick = Joystick()
    joystick.add_handler(joystick.B_BUTTON, lambda state: print('B', state))
    joystick.add_handler(joystick.A_BUTTON, lambda state: print('A', state))
    joystick.add_handler(joystick.X_BUTTON, lambda state: print('X', state))
    joystick.add_handler(joystick.Y_BUTTON, lambda state: print('Y', state))
    joystick.add_handler(joystick.X_AXIS, lambda state: print('X1', state))
    joystick.add_handler(joystick.LEFT_STICK_Y_AXIS, lambda state: print('Y1', state))
    joystick.add_handler(joystick.RIGHT_STICK_X_AXIS, lambda state: print('X2', state))
    joystick.add_handler(joystick.RIGHT_STICK_Y_AXIS, lambda state: print('Y2', state))
    joystick.add_handler(joystick.DPAD_X_AXIS, lambda state: print('DPAD X', state))
    joystick.add_handler(joystick.DPAD_Y_AXIS, lambda state: print('DPAD Y', state))
    joystick.start()
