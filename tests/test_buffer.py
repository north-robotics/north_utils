from unittest import TestCase
import time
from north_utils.test import assert_equal, assert_float_equal
from north_utils.buffer import QueueBuffer


def test_buffer_timeout(buffer, num_bytes, timeout):
    start_time = time.time()
    data = buffer.read(num_bytes, timeout=timeout)
    duration = time.time() - start_time
    assert_float_equal(duration, timeout, timeout / 10 + 0.02)
    return data

class BufferTestCase(TestCase):
    def test_queue_buffer(self):
        buffer = QueueBuffer()

        buffer.write(b'1234')
        assert_equal(buffer.read_all(), b'1234')

        data = test_buffer_timeout(buffer, 2, 0.0)
        assert_equal(data, b'')

        test_buffer_timeout(buffer, 1, 1.0)

        buffer.write(b'abcd')
        data = test_buffer_timeout(buffer, 1024, 0.0)
        assert_equal(data, b'abcd')

        buffer.write(b'567')
        data = test_buffer_timeout(buffer, 4, 1.0)
        assert_equal(data, b'567')

