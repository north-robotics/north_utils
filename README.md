# North Utils

The `north_utils` library contains small utility methods and classes shared across the North Robotics libraries.